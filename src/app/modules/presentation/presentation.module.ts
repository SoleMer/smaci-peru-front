import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { CoreModule } from '../core/core.module';
import { FlexLayoutModule, FlexModule } from '@angular/flex-layout';
import { FooterComponent } from './components/footer/footer.component';


@NgModule({
	declarations: [
		MainLayoutComponent,
		FooterComponent,
	],
	imports: [
		CommonModule,
		CoreModule,
		FlexModule,
		FlexLayoutModule,
	],
	exports: [
		FlexModule,
		FlexLayoutModule,
		MainLayoutComponent,
		FooterComponent,
	]
})
export class PresentationModule {
}
