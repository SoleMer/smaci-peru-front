import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { User } from '../../../../model/user';
import { AuthService } from '../../../api-rest/services/auth.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	email = new FormControl('', [Validators.required, Validators.email]);
	hide = true;

	user: User = {
		email: '',
		password: '',
	};
	loading: boolean = false;

	constructor(private authSvc: AuthService) {
	}

	getErrorMessage() {
		if (this.email.hasError('required')) {
			return 'You must enter a value';
		}

		return this.email.hasError('email') ? 'Not a valid email' : '';
	}

	login() {
		this.loading = true;
		this.authSvc.login(this.user)
			.subscribe(res => {
				console.log(res);
				this.loading = false;
			})
	}

	ngOnInit(): void {
	}

}
