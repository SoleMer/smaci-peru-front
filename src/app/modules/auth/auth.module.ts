import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './pages/login/login.component';
import { CoreModule } from '../core/core.module';


@NgModule({
	declarations: [
		LoginComponent,
	],
	imports: [
		AuthRoutingModule,
		CommonModule,
		CoreModule,
	]
})
export class AuthModule {
}
