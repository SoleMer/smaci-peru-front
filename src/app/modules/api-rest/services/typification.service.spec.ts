import { TestBed } from '@angular/core/testing';

import { TypificationService } from './typification.service';

describe('TypificationService', () => {
  let service: TypificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TypificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
