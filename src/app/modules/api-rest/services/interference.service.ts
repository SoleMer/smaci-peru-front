import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { InterferenceCategory, InterferenceSubCategory } from 'src/app/model/interference';

const INTERFERENCE_CATEGORIES: InterferenceCategory[] = [
	{
		id: 1,
		description: 'Apoyo',
	},
	{
		id: 2,
		description: 'Contra la integridad fisica o la propiedad',
	},
	{
		id: 3,
		description: 'Operativos',
	},
	{
		id: 4,
		description: 'Orden publico',
	},
];

const INTERFERENCE_SUBCATEGORIES: InterferenceSubCategory[] = [
	{
		id: 1,
		description: 'Apoyo fuerza',
		categoryId: 1,
	},
	{
		id: 2,
		description: 'Auxilio vial',
		categoryId: 1,
	},
	{
		id: 3,
		description: 'Rescate de personas',
		categoryId: 1,
	},
	{
		id: 4,
		description: 'Apoyo medico',
		categoryId: 1,
	},
	{
		id: 5,
		description: 'Hallazgo de cadaver',
		categoryId: 2,
	},
	{
		id: 6,
		description: 'Intento de suicidio',
		categoryId: 2,
	},
	{
		id: 7,
		description: 'Uso de productos pirotecnicos sin autorizacion',
		categoryId: 2,
	},
	{
		id: 8,
		description: 'Ataque de animal',
		categoryId: 2,
	},
	{
		id: 9,
		description: 'Vía publica',
		categoryId: 3,
	},
	{
		id: 10,
		description: 'Entidades privadas',
		categoryId: 3,
	},
	{
		id: 11,
		description: 'Entidades publicas',
		categoryId: 3,
	},
	{
		id: 12,
		description: 'Centro educativo',
		categoryId: 3,
	},
	{
		id: 13,
		description: 'Desorden en la via publica',
		categoryId: 4,
	},
	{
		id: 14,
		description: 'Mascotas sin medidas de seguridad',
		categoryId: 4,
	},
	{
		id: 15,
		description: 'Lavado de vehiculo en via publica',
		categoryId: 4,
	},
	{
		id: 16,
		description: 'Poda de arboles y jardines',
		categoryId: 4,
	},
];

@Injectable({
  providedIn: 'root'
})
export class InterferenceService {

  constructor() { }

  getCategories(): Observable<InterferenceCategory[]> {
		return of(INTERFERENCE_CATEGORIES);
	}

	getSubCategoriesByCategoryId(categoryId: number): Observable<InterferenceSubCategory[]> {
		let filteredSubCategories = INTERFERENCE_SUBCATEGORIES.filter(element => element.categoryId === categoryId);
		return of(filteredSubCategories);
	}

	getCategoryById(id: number): any {
		return INTERFERENCE_CATEGORIES.find(s => s.id === id);
	}

	getSubCategoryById(id: number): any {
		return INTERFERENCE_SUBCATEGORIES.find(s => s.id === id);
	}
}
