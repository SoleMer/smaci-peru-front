import { Injectable } from '@angular/core';
import { Operator } from 'src/app/model/operator';

const OPERATORS: Operator[] = [
	{
    id: 1,
		name: 'Juan',
    lastname: 'Perez',
    typeDocument: '', 
    numberDocument: '', 
    email: '',
	},
	{
    id: 2,
		name: 'María',
    lastname: 'Lopez',
    typeDocument: '', 
    numberDocument: '', 
    email: '',
	},
  {
    id: 3,
		name: 'Roberto',
    lastname: 'Gomez',
    typeDocument: '', 
    numberDocument: '', 
    email: '',
	},
];

@Injectable({
  providedIn: 'root'
})
export class OperatorService {

  constructor() { }
  
  getOperatorById(id: number): any {
    return OPERATORS.find(o => o.id === id);
	}
  
}