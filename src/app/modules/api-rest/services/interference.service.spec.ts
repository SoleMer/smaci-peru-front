import { TestBed } from '@angular/core/testing';

import { InterferenceService } from './interference.service';

describe('InterferenceService', () => {
  let service: InterferenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InterferenceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
