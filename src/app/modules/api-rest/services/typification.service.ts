import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { RiskFactorCategory, RiskFactorSubCategory } from '../../../model/risk-factor';

const TYPIFICATION_CATEGORIES: RiskFactorCategory[] = [
	{
		id: 1,
		description: 'Actividades económicas informales, ilegales o de riesgo',
	},
	{
		id: 2,
		description: 'Actividades ilegales o conductas de riesgo',
	},
	{
		id: 3,
		description: 'Afectaciones a las normas de tránsito',
	},
	{
		id: 4,
		description: 'Ambientales y desastres',
	},
];

const TYPIFICATION_SUBCATEGORIES: RiskFactorSubCategory[] = [
	{
		id: 1,
		description: 'Espectáculo público sin permiso',
		categoryId: 1,
	},
	{
		id: 2,
		description: 'Lugar de venta de alimentos sin control sanitario',
		categoryId: 1,
	},
	{
		id: 3,
		description: 'Lugar de venta de drogas',
		categoryId: 1,
	},
	{
		id: 4,
		description: 'Venta de licor fuera de horarios regulados',
		categoryId: 1,
	},
	{
		id: 5,
		description: 'Reciclador de basura no autorizado',
		categoryId: 2,
	},
	{
		id: 6,
		description: 'Persona sospechosa',
		categoryId: 2,
	},
	{
		id: 7,
		description: 'Vehículo sospechoso',
		categoryId: 2,
	},
	{
		id: 8,
		description: 'Pandilla',
		categoryId: 2,
	},
	{
		id: 9,
		description: 'Vía con señalización inadecuada o sin señalización',
		categoryId: 3,
	},
	{
		id: 10,
		description: 'Obstrucción en la vía pública',
		categoryId: 3,
	},
	{
		id: 11,
		description: 'Cierre de vías públicas sin autorización municipal para eventos',
		categoryId: 3,
	},
	{
		id: 12,
		description: 'Paneles publicitarios en la vía pública no autorizados',
		categoryId: 3,
	},
	{
		id: 13,
		description: 'Derrumbe',
		categoryId: 4,
	},
	{
		id: 14,
		description: 'Inundación',
		categoryId: 4,
	},
	{
		id: 15,
		description: 'Humos y olores molestos',
		categoryId: 4,
	},
	{
		id: 16,
		description: 'Aniego y/o problema de desagüe',
		categoryId: 4,
	},
];

@Injectable({
	providedIn: 'root'
})
export class TypificationService {

	constructor() {
	}

	getCategories(): Observable<RiskFactorCategory[]> {
		return of(TYPIFICATION_CATEGORIES);
	}

	getSubCategoriesByCategoryId(categoryId: number): Observable<RiskFactorSubCategory[]> {
		let filteredSubCategories = TYPIFICATION_SUBCATEGORIES.filter(element => element.categoryId === categoryId);
		return of(filteredSubCategories);
	}

	getCategoryById(id: number): any {
		return TYPIFICATION_CATEGORIES.find(s => s.id === id);
	}

	getSubCategoryById(id: number): any {
		return TYPIFICATION_SUBCATEGORIES.find(s => s.id === id);
	}

}
