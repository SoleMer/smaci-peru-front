import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/app/model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  users: User[] = [
    {
      email: 'user1@gmail.com',
      password: '1234',
    },
    {
      email: 'user2@gmail.com',
      password: '1234',
    },
    {
      email: 'user3@gmail.com',
      password: '1234',
    },
    {
      email: 'user4@gmail.com',
      password: '1234',
    },
    {
      email: 'user5@gmail.com',
      password: '1234',
    },
  ]

  private _currentUser: User = { email : '', password: '', };
  currentUser: BehaviorSubject <User> = new BehaviorSubject(this._currentUser);

  constructor() { }

  public login(user: User): Observable<any> {
    console.log(user);
    return this.getUser(user);
  }

  public getUser(user:User) : Observable<User> {
    this.users.forEach(u => {
      if(u.email === user.email && u.password === user.password){
        this._currentUser = u;
        this.currentUser.next(this._currentUser);
      }
    });
    return this.currentUser;
  }
}
