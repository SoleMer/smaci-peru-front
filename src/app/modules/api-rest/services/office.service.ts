import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Office } from 'src/app/model/office';
import { OfficeForEvent } from 'src/app/model/officeForEvent';

const OFFICES: Office[] = [
	{
		id: 1,
		name: 'Policía',
	},
	{
		id: 2,
		name: 'Bomberos',
	},
  {
		id: 3,
		name: 'Defensa civil',
	},
];

const OFFICES_FOR_EVENT: OfficeForEvent[] = [
	{
		idOffice: 1,
		idEvent: 1,
	},
	{
		idOffice: 2,
		idEvent: 1,
	},
	{
		idOffice: 1,
		idEvent: 2,
	},
	{
		idOffice: 3,
		idEvent: 3,
	},
	{
		idOffice: 2,
		idEvent: 4,
	},
	{
		idOffice: 3,
		idEvent: 4,
	},
]

@Injectable({
  providedIn: 'root'
})
export class OfficeService {

  constructor() { }

  getOffices(): Observable<Office[]> {
		return of(OFFICES);
	}

	getOfficesById(officesId: number[]): any {
		let filteredOffices: Office[] = [];
		OFFICES.forEach(office => {
			officesId.forEach(oId => {
				if (office.id == oId) {
					filteredOffices.push(office);
				}
			});
		});
		return filteredOffices;
	}

	getOfficesForEvent(idEvent: number): any[] {
		let offices: any[] = [];
		OFFICES_FOR_EVENT.filter(o => o.idEvent === idEvent).forEach(office => {
			let officeFinded = OFFICES.find(o => o.id === office.idOffice)
			offices.push(officeFinded);
		});
		return offices;
	}



}
