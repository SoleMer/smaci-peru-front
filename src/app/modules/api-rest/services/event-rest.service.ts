import { Injectable } from '@angular/core';
import { Event } from '../../../model/event';
import { BehaviorSubject, Observable, of } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class EventRestService {

    private _events: Event[] = [
        {
            id: 1,
            date: '2021-06-12',
            time: '17',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            typification: 4,
            subcategoryTft: 1,
            geolocation: 'Address1',
            operator: 1,
            priority: 'Alta',
            resolution: 50,
            report: false,
            interference: 1,
            subcategoryItfc: 3,
            informer: 3,
        },
        {
            id: 2,
            date: '2021-06-13',
            time: '16',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            typification: 3,
            subcategoryTft: 4,
            geolocation: 'Address2',
            operator: 2,
            priority: 'Media',
            resolution: 25,
            report: true,
            interference: 2,
            subcategoryItfc: 7,
            informer: 1,
        },
        {
            id: 3,
            date: '2021-06-13',
            time: '18',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            typification: 1,
            subcategoryTft: 2,
            geolocation: 'Address3',
            operator: 1,
            priority: 'Alta',
            resolution: 25,
            report: true,
            interference: 3,
            subcategoryItfc: 10,
            informer: 2,
        },
        {
            id: 4,
            date: '2021-06-14',
            time: '13',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            typification: 2,
            subcategoryTft: 3,
            geolocation: 'Address4',
            operator: 3,
            priority: 'Media',
            resolution: 0,
            report: false,
            interference: 4,
            subcategoryItfc: 14,
            informer: 3,
        }
    ]

    events: BehaviorSubject<Event[]> = new BehaviorSubject(this._events);
    constructor() { }

    getEvents(): Observable<Event[]> {
        return this.events;
    }

    postEvent(event: Event) {
        this._events.push(event);
        this.events.next(this._events);
    }
}
