import { Injectable } from '@angular/core';
import { Informer } from 'src/app/model/informer';

const INFORMERS: Informer[] = [
	{
    id: 1,
		name: 'Marta',
    lastname: 'Perez',
    typeDocument: '', 
    numberDocument: '', 
    email: '',
	},
	{
    id: 2,
		name: 'José',
    lastname: 'Lopez',
    typeDocument: '', 
    numberDocument: '', 
    email: '',
	},
  {
    id: 3,
		name: 'Liliana',
    lastname: 'Gomez',
    typeDocument: '', 
    numberDocument: '', 
    email: '',
	},
];

@Injectable({
  providedIn: 'root'
})
export class InformerService {

  constructor() { }

  getInformerById(id: number): any {
    return INFORMERS.find(i => i.id === id);
	}
}
