import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventProgressiveViewComponent } from './event-progressive-view.component';

describe('EventProgressiveViewComponent', () => {
  let component: EventProgressiveViewComponent;
  let fixture: ComponentFixture<EventProgressiveViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventProgressiveViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventProgressiveViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
