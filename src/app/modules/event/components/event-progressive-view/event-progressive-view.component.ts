import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { EventService } from '../../services/event.service';
import { Event } from '../../../../model/event';
import { Informer } from 'src/app/model/informer';
import { Office } from 'src/app/model/office';
import { InterferenceCategory, InterferenceSubCategory } from 'src/app/model/interference';
import { RiskFactorCategory, RiskFactorSubCategory } from 'src/app/model/risk-factor';

@Component({
    selector: 'app-event-progressive-view',
    templateUrl: './event-progressive-view.component.html',
    styleUrls: ['./event-progressive-view.component.scss']
})
export class EventProgressiveViewComponent implements OnInit {

    event$!: Event;
    informer$!: Informer;
    offices$!: Office[];
    typification$!: RiskFactorCategory;
    subcategoryTft$!: RiskFactorSubCategory;
    interference$!: InterferenceCategory;
    subcategoryItfc$!: InterferenceSubCategory;
    report$!: string;
    //report: string = ((this.event$.report) ? 'SI' : 'NO');

    constructor(private eventService: EventService) { }

    ngOnInit(): void {
        this.eventService.event.subscribe(e => this.event$ = e);
        this.eventService.informer.subscribe(i => this.informer$ = i);
        this.eventService.offices.subscribe(o => this.offices$ = o);
        this.eventService.typification.subscribe(t => this.typification$ = t);
        this.eventService.subcategoryTft.subscribe(s => this.subcategoryTft$ = s);
        this.eventService.interference.subscribe(i => this.interference$ = i);
        this.eventService.subcategoryItfc.subscribe(s => this.subcategoryItfc$ = s);
        this.eventService.report.subscribe(r => this.report$ = r);
    }
}
