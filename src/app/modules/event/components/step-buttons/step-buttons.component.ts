import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
    selector: 'app-step-buttons',
    templateUrl: './step-buttons.component.html',
    styleUrls: ['./step-buttons.component.scss']
})
export class StepButtonsComponent implements OnInit {

    @Output() continueEmit: EventEmitter<any> = new EventEmitter();
    @Output() backEmit: EventEmitter<any> = new EventEmitter();

    color: string = 'accent';
    constructor() { }

    ngOnInit(): void {
    }

    clickContinue(event: any) {
        this.continueEmit.emit(event);
    }

    clickBack(event: any) {
        this.backEmit.emit(event);
    }

}
