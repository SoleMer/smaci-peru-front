import { Component, Input, OnInit } from '@angular/core';
import { Event, EventShow } from 'src/app/model/event';
import { ShowEventService } from '../../services/show-event.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EventService } from '../../services/event.service';

@Component({
    selector: 'app-similar-events',
    templateUrl: './similar-events.component.html',
    styleUrls: ['./similar-events.component.scss']
})
export class SimilarEventsComponent implements OnInit {

    title: string = '';
    events: EventShow[] = [];
    filter!: FormGroup;
    @Input() search!: any;

    constructor(private showEventService: ShowEventService,
        private eventService: EventService) { }

    ngOnInit(): void {
        this.eventService.messageSimilarEvents.subscribe(m => this.title = m);
        this.events = this.showEventService.getEvents();
        this.showEventService.similarEvents.subscribe(e => this.events = e);
    }

    manageAssociate(id: number) {
        this.eventService.manageAssociate(id);
        this.events.forEach(e => {
            if (e.id === id) { e.associate = !e.associate; }
        });
    }

}
