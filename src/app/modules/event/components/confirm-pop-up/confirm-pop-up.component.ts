import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EventService } from '../../services/event.service';
import { Router } from '@angular/router';
import { ChronometerService } from '../../services/chronometer.service';


@Component({
    selector: 'app-confirm-pop-up',
    templateUrl: './confirm-pop-up.component.html',
    styleUrls: ['./confirm-pop-up.component.scss']
})
export class ConfirmPopUpComponent implements OnInit {

    timer!: any;
    action!: string;
    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        private eventService: EventService,
        private router: Router,
        private chronometerService: ChronometerService) {
        this.action = data.action;
    }

    ngOnInit(): void {
    }

    save() {
        this.chronometerService.saveTime()
        this.chronometerService.saveTime();
        this.eventService.saveEvent();
        this.timer = setTimeout(() => {
            this.redirect(`event/list`);
        }, 100);
    }

    cancel() {
        this.chronometerService.clearChronometer();
        this.eventService.resetEvent();
        this.timer = setTimeout(() => {
            this.redirect(`event/list`);
        }, 100);
    }

    redirect(path: string) {
        this.router.navigate([path]);
    }

    continue() {
        this.chronometerService.toogleChronometer();
    }
}
