import { Component, OnInit } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Office } from 'src/app/model/office';
import { EventRestService } from 'src/app/modules/api-rest/services/event-rest.service';
import { InformerService } from 'src/app/modules/api-rest/services/informer.service';
import { InterferenceService } from 'src/app/modules/api-rest/services/interference.service';
import { OfficeService } from 'src/app/modules/api-rest/services/office.service';
import { OperatorService } from 'src/app/modules/api-rest/services/operator.service';
import { TypificationService } from 'src/app/modules/api-rest/services/typification.service';
import { Event, EventShow } from 'src/app/model/event';
import { Operator } from 'src/app/model/operator';
import { Informer } from 'src/app/model/informer';
import { ShowEventService } from '../../services/show-event.service';

@Component({
    selector: 'app-event-log',
    templateUrl: './event-log.component.html',
    styleUrls: ['./event-log.component.scss']
})
export class EventLogComponent implements OnInit, AfterViewInit {

    showEvents: EventShow[] = [];

    displayedColumns: string[] = ['operator', 'date', 'typification', 'description', 'geolocation', 'informer', 'dispatch', 'priority', 'resolution'];
    dataSource: any;


    @ViewChild(MatPaginator) paginator!: MatPaginator;

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }


    filter!: FormGroup;

    constructor(private formBuilder: FormBuilder,
        private showEventService: ShowEventService) { }


    ngOnInit(): void {
        this.showEvents = this.showEventService.getEvents();
        this.dataSource = new MatTableDataSource<EventShow>(this.showEvents);
        this.filter = this.formBuilder.group({
            search: [null, null],
        });
    }

    applyFilter() {
        this.dataSource.filter = this.filter.value.search.trim().toLowerCase();
    }


}
