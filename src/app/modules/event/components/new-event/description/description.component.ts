import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventService } from '../../../services/event.service';

@Component({
    selector: 'app-description',
    templateUrl: './description.component.html',
    styleUrls: ['./description.component.scss']
})
export class DescriptionComponent implements OnInit {

    @Output() slipEmit: EventEmitter<string> = new EventEmitter();
    next: string = "event/#typification";
    description!: FormGroup;

    constructor(private formBuilder: FormBuilder,
        private eventService: EventService) {
    }


    ngOnInit(): void {
        this.description = this.formBuilder.group({
            detail: [null, Validators.required],
        });
    }

    continue(event: any) {
        if (this.description.value.detail != null) {
            this.eventService.setDescription(this.description.value.detail);
        }
        this.slipEmit.emit('typification');
    }

    back(event: any) {
        this.slipEmit.emit('datetime');
    }
}
