import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import { NgModule } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { EventService } from '../../../services/event.service';

@Component({
    selector: 'app-geolocation',
    templateUrl: './geolocation.component.html',
    styleUrls: ['./geolocation.component.scss']
})

export class GeolocationComponent implements PipeTransform {
    @Pipe({ name: 'safe' })
    //la url se forma por tres partes: urlPartOne, address y urlPartThree
    urlPartOne: string = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3136.9537258949276!2d-58.7819597869984!3d-38.16451496196313!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x958fdfa919fa9153%3A0x6239e45280390a80!2s';
    urlPartThree: string = '!5e0!3m2!1ses!2sar!4v1623414268677!5m2!1ses!2sar';
    address!: string;
    urlCompleted: string = this.urlPartOne.concat(this.address, this.urlPartThree);
    currentUrl!: SafeUrl;
    search: string = '';
    @Output() slipEmit: EventEmitter<string> = new EventEmitter();

    constructor(private sanitizer: DomSanitizer,
        private evetService: EventService) {
    }

    //por cada tecla presionada se activa setAddress()
    setAddress() {    //se quitan los espacios de lo ingresado en el input y se guarda en address
        this.address = this.search.replace(/\s+/g, '%20');
        this.urlCompleted = this.urlPartOne.concat(this.address, this.urlPartThree);
        this.transform();
    }

    transform() {     //currentUrl debe ser un src seguro para que lo acepte el iframe
        this.currentUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.urlCompleted);
    }

    ngOnInit(): void {
        //Se le da una primer direccion para mostrar. Va a ser la dir. del ultimo evento guardado
        this.address = 'Italia%20373%2C%20Lober%C3%ADa%2C%20Provincia%20de%20Buenos%20Aires';
        this.transform();
    }

    continue(event: any) {
        this.evetService.setGeolocation(this.search);
        this.slipEmit.emit('informer');
    }

    back(event: any) {
        this.slipEmit.emit('typification');
    }
}

