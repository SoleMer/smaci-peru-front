import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RiskFactorCategory, RiskFactorSubCategory } from 'src/app/model/risk-factor';
import { TypificationService } from '../../../../api-rest/services/typification.service';
import { MatOptionSelectionChange } from '@angular/material/core';
import { EventService } from '../../../services/event.service';
import { Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'app-typification',
	templateUrl: './typification.component.html',
	styleUrls: ['./typification.component.scss']
})
export class TypificationComponent implements OnInit {

	@Output() slipEmit: EventEmitter<string> = new EventEmitter();
	public categories: RiskFactorCategory[] = [];
	public subcategories: RiskFactorSubCategory[] = [];

	public typificationForm!: FormGroup;

	constructor(private formBuilder: FormBuilder,
				private typificationService: TypificationService,
				private eventService: EventService) {
	}

	ngOnInit(): void {
		this.typificationForm = this.formBuilder.group({
			categoryId: [null, Validators.required],
			subCategoryId: [null, Validators.required]
		});
		this.typificationForm.controls.subCategoryId.disable();
		this.typificationService.getCategories().subscribe(data => {this.categories = data});

	}

	getSubCategoriesByCategoryId(event: MatOptionSelectionChange, categoryId: number): void {
		this.typificationService.getSubCategoriesByCategoryId(categoryId)
		.subscribe(data => {
			this.subcategories = data;
			this.typificationForm.controls.subCategoryId.enable();
		});
	}
	
	continue(event: any) {
	  this.eventService.setTypification(this.typificationForm.value);
	  this.slipEmit.emit('geolocation');
  }

  back(event: any) {
    this.slipEmit.emit('description');
  }
}
