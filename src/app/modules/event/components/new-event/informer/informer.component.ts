import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { EventService } from '../../../services/event.service';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

@Component({
    selector: 'app-informer',
    templateUrl: './informer.component.html',
    styleUrls: ['./informer.component.scss']
})
export class InformerComponent implements OnInit {

    informerForm!: FormGroup;
    @Output() slipEmit: EventEmitter<string> = new EventEmitter();
    /*
      emailFormControl = new FormControl('', [
        Validators.required,
        Validators.email,
      ]);
    */
    matcher = new MyErrorStateMatcher();

    constructor(private formBuilder: FormBuilder,
        private eventService: EventService) { }

    ngOnInit(): void {
        this.informerForm = this.formBuilder.group({
            name: [null, Validators.required],
            lastname: [null, Validators.required],
            typeDocument: [null, Validators.required],
            numberDocument: [null, Validators.required],
            email: [null, Validators.required, Validators.email],
        })
    }

    continue(event: any) {
        this.eventService.setInformer(this.informerForm.value);
        this.slipEmit.emit('resolution');
    }

    back(event: any) {
        this.slipEmit.emit('geolocation');
    }
}
