import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOptionSelectionChange } from '@angular/material/core';
import { InterferenceCategory, InterferenceSubCategory } from 'src/app/model/interference';
import { Office } from 'src/app/model/office';
import { InterferenceService } from 'src/app/modules/api-rest/services/interference.service';
import { OfficeService } from 'src/app/modules/api-rest/services/office.service';
import { EventService } from '../../../services/event.service';

@Component({
    selector: 'app-dispatch',
    templateUrl: './dispatch.component.html',
    styleUrls: ['./dispatch.component.scss']
})
export class DispatchComponent implements OnInit {

    @Output() slipEmit: EventEmitter<string> = new EventEmitter();
    officeSelected: String = '';
    public offices: Office[] = [];
    public categories: InterferenceCategory[] = [];
    public subcategories: InterferenceSubCategory[] = [];

    dispatchForm!: FormGroup;

    constructor(private formBuilder: FormBuilder,
        private interferenceService: InterferenceService,
        private officeService: OfficeService,
        private eventService: EventService) {
    }

    ngOnInit(): void {
        this.dispatchForm = this.formBuilder.group({
            offices: this.formBuilder.array([], [Validators.required]),
            categoryId: [null, Validators.required],
            subCategoryId: [null]
        });
        this.dispatchForm.controls.subCategoryId.disable();
        this.interferenceService.getCategories().subscribe(data => { this.categories = data });
        this.officeService.getOffices().subscribe(data => { this.offices = data });
    }

    onCbChange(e: any, id: number) {
        const offices: FormArray = this.dispatchForm.get('offices') as FormArray;

        if (e.checked) {
            offices.push(new FormControl(id));
        } else {
            let i: number = 0;
            offices.controls.forEach(item => {
                if (item.value == id) {
                    offices.removeAt(i);
                    return;
                }
                i++;
            });
        }
    }

    getSubCategoriesByCategoryId(event: MatOptionSelectionChange, categoryId: number): void {
        this.interferenceService.getSubCategoriesByCategoryId(categoryId)
            .subscribe(data => {
                this.subcategories = data;
                this.dispatchForm.controls.subCategoryId.enable();
            });
    }

    continue(event: any) {
        this.eventService.setDispatch(this.dispatchForm.value);
    }

    back(event: any) {
        this.slipEmit.emit('resolution');
    }
}
