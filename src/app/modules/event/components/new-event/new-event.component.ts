import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ChronometerService } from '../../services/chronometer.service';
import { EventService } from '../../services/event.service';
import { ConfirmPopUpComponent } from '../confirm-pop-up/confirm-pop-up.component';

@Component({
    selector: 'app-new-event',
    templateUrl: './new-event.component.html',
    styleUrls: ['./new-event.component.scss']
})
export class NewEventComponent implements OnInit {

    firstFormGroup!: FormGroup;
    secondFormGroup!: FormGroup;
    filter: string = '';

    constructor(private _formBuilder: FormBuilder,
        private eventService: EventService,
        private matDialog: MatDialog,
        private chronometerService: ChronometerService) { }

    ngOnInit(): void {
        this.firstFormGroup = this._formBuilder.group({
            firstCtrl: ['', Validators.required]
        });
        this.secondFormGroup = this._formBuilder.group({
            secondCtrl: ['', Validators.required]
        });
    }

    slip(path: string) {
        let element = document.getElementById(path);
        if (element) {
            element.scrollIntoView({ behavior: 'smooth' });
        }
    }

    openDialog(action: string) {
        this.matDialog.open(ConfirmPopUpComponent, {
            data: {
                action: action,
            }
        });
        this.chronometerService.toogleChronometer();
    }

    cancel() {
        this.openDialog("eliminar");
    }

    save() {
        this.openDialog("guardar");
    }

}
