import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventService } from '../../../services/event.service';

@Component({
    selector: 'app-datetime',
    templateUrl: './datetime.component.html',
    styleUrls: ['./datetime.component.scss']
})
export class DatetimeComponent implements OnInit {

    @Output() slipEmit: EventEmitter<string> = new EventEmitter();
    public datetime!: FormGroup;

    constructor(private formBuilder: FormBuilder,
        private eventService: EventService) {
    }

    ngOnInit(): void {
        this.datetime = this.formBuilder.group({
            date: [null, Validators.required],
            time: [null, Validators.required]
        });

    }

    continue(event: any) {
        if (this.datetime.value.date != null && this.datetime.value.time != null) {
            this.eventService.setDateTime(this.datetime.value);
        }
        this.slipEmit.emit('description');
    }


}
