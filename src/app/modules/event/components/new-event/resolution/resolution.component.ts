import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventService } from '../../../services/event.service';

@Component({
    selector: 'app-resolution',
    templateUrl: './resolution.component.html',
    styleUrls: ['./resolution.component.scss']
})
export class ResolutionComponent implements OnInit {

    levels: String[] = [
        'alta',
        'media',
        'baja',
    ]

    @Output() slipEmit: EventEmitter<string> = new EventEmitter();
    selected: String = '';
    report: boolean = false;

    resolutionForm!: FormGroup;

    constructor(private formBuilder: FormBuilder,
        private eventService: EventService) {
    }

    ngOnInit(): void {
        this.resolutionForm = this.formBuilder.group({
            priority: [null, Validators.required],
            report: [null],
            levelResolution: [null],
        });
    }

    setSelected() {
        this.selected = this.resolutionForm.value.priority;
    }

    toggleReport() {
        this.report = !this.report;
    }

    formatLabel(value: number) {
        return value + '%';
    }


    continue(event: any) {
        this.eventService.setResolution(this.resolutionForm.value);
        this.slipEmit.emit('dispatch');
    }

    back(event: any) {
        this.slipEmit.emit('informer');
    }

}
