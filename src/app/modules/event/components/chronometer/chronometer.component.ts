import { Component, OnInit } from '@angular/core';
import { ChronometerService } from '../../services/chronometer.service';

@Component({
    selector: 'app-chronometer',
    templateUrl: './chronometer.component.html',
    styleUrls: ['./chronometer.component.scss']
})
export class ChronometerComponent implements OnInit {

    minutes: number = 0;
    seconds: number = 0;
    strgSeconds: string = '00';
    loadingEvent!: boolean;
    saving!: boolean;
    clear!: boolean;
    increment: any;

    constructor(private chronometerService: ChronometerService) { }

    ngOnInit(): void {
        this.chronometerService.loadingEvent.subscribe(l => this.loadingEvent = l);
        this.chronometerService.saving.subscribe(s => this.saving = s);
        this.chronometerService.clear.subscribe(c => this.clear = c);
        this.chronometerService.startChronometer();
        this.increaseSeconds();
    }

    increaseSeconds(): void {
        this.increment = setInterval(() => {
            if (this.loadingEvent) {
                if (this.seconds < 59) {
                    this.seconds++;
                } else {
                    this.seconds = 0;
                    this.minutes++;
                }
                if (this.seconds < 10) {
                    this.strgSeconds = '0' + this.seconds;
                } else {
                    this.strgSeconds = this.seconds.toString();
                }
            } else {
                if (this.clear) {
                    if (this.saving) {
                        this.chronometerService.saveTimeEvent(this.minutes + ':' + this.strgSeconds);
                    }
                    this.seconds = 0;
                    this.minutes = 0;
                    this.strgSeconds = '00';
                    clearInterval(this.increment);
                }
            }
        }, 1000);

    }

    ngOnDestroy() {
        this.seconds = 0;
        this.minutes = 0;
        this.strgSeconds = '00';
        clearInterval(this.increment);
    }

}
