import { Injectable } from '@angular/core';
import { BehaviorSubject, of } from 'rxjs';
import { Informer } from 'src/app/model/informer';
import { InterferenceCategory, InterferenceSubCategory } from 'src/app/model/interference';
import { Office } from 'src/app/model/office';
import { RiskFactorCategory, RiskFactorSubCategory } from 'src/app/model/risk-factor';
import { Event } from '../../../model/event';
import { EventRestService } from '../../api-rest/services/event-rest.service';
import { InterferenceService } from '../../api-rest/services/interference.service';
import { OfficeService } from '../../api-rest/services/office.service';
import { TypificationService } from '../../api-rest/services/typification.service';
import { NewEventComponent } from '../components/new-event/new-event.component';
import { ShowEventService } from './show-event.service';


@Injectable({
    providedIn: 'root'
})
export class EventService {

    associateEvents: number[] = [];

    private _event: Event = {
        date: '',
        time: '',
        description: '',
        typification: 0,
        subcategoryTft: 0,
        geolocation: '',
        operator: 0,
        priority: '',
        resolution: 0,
        report: false,
        interference: 0,
        subcategoryItfc: 0,
        loadingTime: '0:00'
    };
    event: BehaviorSubject<Event> = new BehaviorSubject(this._event);

    officesId: number[] = [];
    private _offices: Office[] = [];

    offices: BehaviorSubject<Office[]> = new BehaviorSubject(this._offices);

    private _informer: Informer = {
        id: 0,
        name: '',
        lastname: '',
        typeDocument: '',
        numberDocument: '',
        email: '',
    }

    private _typification: RiskFactorCategory = { id: 0, description: '' };
    private _subCategoryTft: RiskFactorSubCategory = { id: 0, description: '', categoryId: 0 };
    private _interference: InterferenceCategory = { id: 0, description: '' };
    private _subCategoryItfc: InterferenceSubCategory = { id: 0, description: '', categoryId: 0 };
    private _report: string = '';
    private _messageSimilarEvents: string = 'Eventos recientes';

    typification: BehaviorSubject<RiskFactorCategory> = new BehaviorSubject(this._typification);
    subcategoryTft: BehaviorSubject<RiskFactorSubCategory> = new BehaviorSubject(this._subCategoryTft);
    interference: BehaviorSubject<InterferenceCategory> = new BehaviorSubject(this._interference);
    subcategoryItfc: BehaviorSubject<InterferenceSubCategory> = new BehaviorSubject(this._subCategoryItfc);
    report: BehaviorSubject<string> = new BehaviorSubject(this._report);
    informer: BehaviorSubject<Informer> = new BehaviorSubject(this._informer);
    messageSimilarEvents: BehaviorSubject<string> = new BehaviorSubject(this._messageSimilarEvents);

    constructor(private officeService: OfficeService,
        private typificationService: TypificationService,
        private interferenceService: InterferenceService,
        private eventRestService: EventRestService,
        private showEventService: ShowEventService) { }

    setDateTime(datetime: any) {
        this._event.date = datetime.date;
        this._event.time = datetime.time;
        this.event.next(this._event);
        this._messageSimilarEvents = 'Eventos para esta fecha';
        this.messageSimilarEvents.next(this._messageSimilarEvents);
        this.showEventService.filterEventsByDate(this._event.date);
    }

    setDescription(description: string) {
        this._event.description = description;
        this.event.next(this._event);
    }

    setTypification(typification: any) {
        this._event.typification = typification.categoryId;
        this._event.subcategoryTft = typification.subCategoryId;
        this.event.next(this._event);
        this._typification = this.typificationService.getCategoryById(typification.categoryId);
        this._subCategoryTft = this.typificationService.getSubCategoryById(typification.subCategoryId);
        this.typification.next(this._typification);
        this.subcategoryTft.next(this._subCategoryTft);
        this.showEventService.filterEventsByTypificationCategory(this._typification);
        this._messageSimilarEvents = 'Eventos de esta categoría';
        this.messageSimilarEvents.next(this._messageSimilarEvents);
    }

    setGeolocation(address: string) {
        this._event.geolocation = address;
        this.event.next(this._event);
        this.showEventService.filterEventsByGeolocation(address);
        this._messageSimilarEvents = 'Eventos en esta locación';
        this.messageSimilarEvents.next(this._messageSimilarEvents);
    }

    setInformer(informer: Informer) {
        //The _informer should be saved and then the id of the _informer should be saved in the event
        this._informer.name = informer.name;
        this._informer.lastname = informer.lastname;
        this._informer.typeDocument = informer.typeDocument;
        this._informer.numberDocument = informer.numberDocument;
        this._informer.email = informer.email;
        this.informer.next(this._informer);
    }

    setOperator() {
        this._event.operator = 0; //get from user
        this.event.next(this._event);
    }

    setResolution(resolution: any) {
        this._event.priority = resolution.priority;
        this._event.resolution = resolution.levelResolution;
        this._event.report = resolution.report;
        this.event.next(this._event);
        if (this._event.report) { this._report = 'SI'; }
        else { this._report = 'NO'; }
        this.report.next(this._report);
    }

    setDispatch(dispatch: any) {
        this.officesId = dispatch.offices;
        this._event.interference = dispatch.categoryId;
        this._event.subcategoryItfc = dispatch.subCategoryId;
        this._offices = this.officeService.getOfficesById(this.officesId);
        this.offices.next(this._offices);
        this.event.next(this._event);
        this._interference = this.interferenceService.getCategoryById(dispatch.categoryId);
        this._subCategoryItfc = this.interferenceService.getSubCategoryById(dispatch.subCategoryId);
        this.interference.next(this._interference);
        this.subcategoryItfc.next(this._subCategoryItfc);
    }

    setLoadingTime(loadingTime: string) {
        this._event.loadingTime = loadingTime;
    }

    resetEvent() {
        this._event = {
            date: '',
            time: '',
            description: '',
            typification: 0,
            subcategoryTft: 0,
            geolocation: '',
            operator: 0,
            priority: '',
            resolution: 0,
            report: false,
            interference: 0,
            subcategoryItfc: 0,
            loadingTime: '0:00',
        };
        this._typification = { id: 0, description: '' };
        this._subCategoryTft = { id: 0, description: '', categoryId: 0 };
        this._interference = { id: 0, description: '' };
        this._subCategoryItfc = { id: 0, description: '', categoryId: 0 };
        this._report = '';
        this._offices = [];
        this._informer = {
            id: 0,
            name: '',
            lastname: '',
            typeDocument: '',
            numberDocument: '',
            email: '',
        }

        this.event.next(this._event);
        this.typification.next(this._typification);
        this.subcategoryTft.next(this._subCategoryTft);
        this.interference.next(this._interference);
        this.subcategoryItfc.next(this._subCategoryItfc);
        this.report.next(this._report);
        this.offices.next(this._offices);
        this.informer.next(this._informer);

        this.showEventService.getEvents();
        this._messageSimilarEvents = 'Eventos recientes';
        this.messageSimilarEvents.next(this._messageSimilarEvents);
    }

    saveEvent() {
        this.eventRestService.postEvent(this._event);
        this.resetEvent();
    }

    manageAssociate(id: number) {
        if (this.associateEvents.includes(id)) {
            this.associateEvents.splice(this.associateEvents.indexOf(id), 1);
        } else {
            this.associateEvents.push(id);
        }
    }
}
