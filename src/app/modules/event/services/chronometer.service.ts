import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { EventService } from './event.service';

@Injectable({
    providedIn: 'root'
})
export class ChronometerService {

    private _loadingEvent: boolean = false;
    loadingEvent: BehaviorSubject<boolean> = new BehaviorSubject(this._loadingEvent);
    private _saving: boolean = false;
    saving: BehaviorSubject<boolean> = new BehaviorSubject(this._saving);
    private _clear: boolean = false;
    clear: BehaviorSubject<boolean> = new BehaviorSubject(this._clear);

    constructor(private eventService: EventService) { }

    startChronometer() {
        console.log("start");
        this.saving.next(false);
        this.clear.next(false);
        this.toogleChronometer();
        console.log(this._loadingEvent);
    }

    toogleChronometer() {
        this._loadingEvent = !this._loadingEvent;
        this.loadingEvent.next(this._loadingEvent);
    }

    saveTime() {
        this.saving.next(true);
        this.clear.next(true);
    }

    clearChronometer() {
        this.clear.next(true);
    }

    saveTimeEvent(loadingTime: string) {
        this.eventService.setLoadingTime(loadingTime);
    }

    stopChronometer() {
        if (this.loadingEvent) {
            this.toogleChronometer();
            this.clearChronometer();
        }
    }
}
