import { Injectable } from '@angular/core';
import { EventRestService } from '../../api-rest/services/event-rest.service';
import { InformerService } from 'src/app/modules/api-rest/services/informer.service';
import { InterferenceService } from 'src/app/modules/api-rest/services/interference.service';
import { OfficeService } from 'src/app/modules/api-rest/services/office.service';
import { OperatorService } from 'src/app/modules/api-rest/services/operator.service';
import { TypificationService } from 'src/app/modules/api-rest/services/typification.service';
import { Event, EventShow } from 'src/app/model/event';
import { Operator } from 'src/app/model/operator';
import { Informer } from 'src/app/model/informer';
import { Office } from 'src/app/model/office';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { RiskFactorCategory } from 'src/app/model/risk-factor';

@Injectable({
    providedIn: 'root'
})
export class ShowEventService {

    events$: Event[] = [];
    showEvents: EventShow[] = [];
    private _similarEvents: EventShow[] = [];
    similarEvents: BehaviorSubject<EventShow[]> = new BehaviorSubject(this._similarEvents);

    constructor(private eventRestService: EventRestService,
        private typificationService: TypificationService,
        private interferenceService: InterferenceService,
        private officeService: OfficeService,
        private operatorService: OperatorService,
        private informerService: InformerService) { }

    getEvents(): EventShow[] {
        this.eventRestService.events.subscribe(e => this.events$ = e);
        this.getDataEvents();
        this._similarEvents = this.showEvents;
        this.similarEvents.next(this._similarEvents);
        return this.showEvents;
    }

    getDataEvents() {
        this.showEvents = [];
        this.events$.forEach(e => {
            let idEvent: number = 0;
            let idInformer: number = 0;
            if (e.id) { idEvent = e.id };
            if (e.informer) { idInformer = e.informer };
            let event: EventShow = {
                id: idEvent,
                date: e.date,
                time: e.time,
                description: e.description,
                typification: this.typificationService.getCategoryById(e.typification).description,
                subcategoryTft: this.typificationService.getSubCategoryById(e.subcategoryTft).description,
                geolocation: e.geolocation,
                operator: this.getOperatorName(e.operator),
                priority: e.priority,
                resolution: e.resolution,
                report: e.report,
                interference: this.interferenceService.getCategoryById(e.interference).description,
                subcategoryItfc: this.interferenceService.getSubCategoryById(e.subcategoryItfc).description,
                offices: this.getOfficesName(idEvent),
                informer: this.getInformerName(idInformer),
                associate: false,
            };
            this.showEvents.push(event);
        });
    }

    getOfficesName(idEvent: number): string[] {
        let offices: Office[] = this.officeService.getOfficesForEvent(idEvent);
        let officesName: string[] = [];
        offices.forEach(office => {
            officesName.push(office.name);
        });
        return officesName;
    }

    getOperatorName(idOperator: number): string {
        let operator: Operator = this.operatorService.getOperatorById(idOperator);
        return operator.name + ' ' + operator.lastname;
    }

    getInformerName(idInformer: number): string {
        let informer: Informer = this.informerService.getInformerById(idInformer);
        return informer.name + ' ' + informer.lastname;
    }

    filterEventsByDate(date: string) {
        let filteredEvents = this.showEvents.filter(event => event.date === date);
        this._similarEvents = filteredEvents;
        this.similarEvents.next(this._similarEvents);
    }

    filterEventsByTypificationCategory(typification: RiskFactorCategory) {
        let filteredEvents = this.showEvents.filter(event => event.typification === typification.description);
        this._similarEvents = filteredEvents;
        this.similarEvents.next(this._similarEvents);
    }

    filterEventsByGeolocation(geolocation: string) {
        let filteredEvents = this.showEvents.filter(event => event.geolocation.toLowerCase() === geolocation.toLowerCase());
        this._similarEvents = filteredEvents;
        this.similarEvents.next(this._similarEvents);
    }
}
