import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventRoutingModule } from './event-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { CoreModule } from '../core/core.module';
import { PresentationModule } from '../presentation/presentation.module';
import { EventLogComponent } from './components/event-log/event-log.component';
import { NewEventComponent } from './components/new-event/new-event.component';
import { DatetimeComponent } from './components/new-event/datetime/datetime.component';
import { DescriptionComponent } from './components/new-event/description/description.component';
import { DispatchComponent } from './components/new-event/dispatch/dispatch.component';
import { GeolocationComponent } from './components/new-event/geolocation/geolocation.component';
import { InformerComponent } from './components/new-event/informer/informer.component';
import { ResolutionComponent } from './components/new-event/resolution/resolution.component';
import { TypificationComponent } from './components/new-event/typification/typification.component';
import { StepButtonsComponent } from './components/step-buttons/step-buttons.component';
import { EventProgressiveViewComponent } from './components/event-progressive-view/event-progressive-view.component';
import { ConfirmPopUpComponent } from './components/confirm-pop-up/confirm-pop-up.component';
import { SimilarEventsComponent } from './components/similar-events/similar-events.component';
import { ChronometerComponent } from './components/chronometer/chronometer.component';


@NgModule({
	declarations: [
		HomeComponent,
		EventLogComponent,
		NewEventComponent,
		DatetimeComponent,
		DescriptionComponent,
		DispatchComponent,
		GeolocationComponent,
		InformerComponent,
		ResolutionComponent,
		TypificationComponent,
  		StepButtonsComponent,
  		EventProgressiveViewComponent,
    ConfirmPopUpComponent,
    SimilarEventsComponent,
    ChronometerComponent,
	],
	imports: [
		CommonModule,
		CoreModule,
		EventRoutingModule,
		PresentationModule,
	],
	exports: [
		EventProgressiveViewComponent,
	]
})
export class EventModule {
}
