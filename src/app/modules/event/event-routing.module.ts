import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { MainLayoutComponent } from '../presentation/components/main-layout/main-layout.component';
import { NewEventComponent } from './components/new-event/new-event.component';
import { EventLogComponent } from './components/event-log/event-log.component';

const routes: Routes = [
	{
		path: '',
		component: HomeComponent,
		children: [
			{
				path: '',
				redirectTo: 'list',
				pathMatch: 'full',
			},
			{
				path: 'new',
				component: NewEventComponent,
			},
			{
				path: 'list',
				component: EventLogComponent,
			}
		]
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class EventRoutingModule {
}
