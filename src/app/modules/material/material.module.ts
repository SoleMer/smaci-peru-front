import { NgModule } from '@angular/core';
import { MatFormField, MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatDividerModule } from '@angular/material/divider';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';

@NgModule({
	declarations: [],
	imports: [
		MatButtonModule,
		MatFormFieldModule,
		MatIconModule,
		MatInputModule,
		MatCardModule,
		FormsModule,
		MatFormFieldModule,
		ReactiveFormsModule,
		MatStepperModule,
		MatSelectModule,
		MatSliderModule,
		MatSlideToggleModule,
		MatCheckboxModule,
		MatSidenavModule,
		MatPaginatorModule,
		MatFormFieldModule,
		MatTableModule,
		MatGridListModule,		
		MatDividerModule,
		MatDialogModule,
		MatExpansionModule,
	],
	exports: [
		MatButtonModule,
		MatFormFieldModule,
		MatIconModule,
		MatInputModule,
		MatCardModule,
		FormsModule,
		ReactiveFormsModule,
		MatStepperModule,
		MatSelectModule,
		MatSliderModule,
		MatSlideToggleModule,
		MatCheckboxModule,
		MatSidenavModule,
		MatPaginatorModule,
		MatFormFieldModule,
		MatTableModule,
		MatGridListModule,
		MatDividerModule,
		MatDialogModule,
		MatExpansionModule,
	]
})
export class MaterialModule {
}
