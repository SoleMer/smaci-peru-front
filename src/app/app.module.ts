import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './modules/auth/pages/login/login.component';
import { NewEventComponent } from './modules/event/components/new-event/new-event.component';
import { DatetimeComponent } from './modules/event/components/new-event/datetime/datetime.component';
import { DescriptionComponent } from './modules/event/components/new-event/description/description.component';
import { TypificationComponent } from './modules/event/components/new-event/typification/typification.component';
import { GeolocationComponent } from './modules/event/components/new-event/geolocation/geolocation.component';
import { InformerComponent } from './modules/event/components/new-event/informer/informer.component';
import { ResolutionComponent } from './modules/event/components/new-event/resolution/resolution.component';
import { DispatchComponent } from './modules/event/components/new-event/dispatch/dispatch.component';
import { MaterialModule } from './modules/material/material.module';
import { PresentationModule } from './modules/presentation/presentation.module';
import { CoreModule } from './modules/core/core.module';


@NgModule({
	declarations: [
		AppComponent,
	],
	imports: [
		AppRoutingModule,
		BrowserModule,
		BrowserAnimationsModule,
		CoreModule,
		MaterialModule,
		PresentationModule,
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
