import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'event',
		pathMatch: 'full',
	},
	{
		path: 'event',
		loadChildren: () => import('./modules/event/event.module').then(m => m.EventModule),
	},
	{
		path: 'login',
		loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule),
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
