import { Informer } from "./informer";
import { Office } from "./office";

export interface Event {
    id?: number;
    date: string;
    time: string;
    description: string;
    typification: number; //risk-factor reference
    subcategoryTft: number; //subcategory reference
    geolocation: string;    //if there are coordinates, geolocation reference
    operator: number; //operator reference
    priority: string;
    resolution: number; //percent
    report: boolean;
    interference: number; //interference reference
    subcategoryItfc: number; //subcategory interference reference
    informer?: number;
    loadingTime?: string;
}

export interface EventShow {
    id: number;
    date: string;
    time: string;
    description: string;
    typification: string;
    subcategoryTft?: string;
    geolocation: string;
    operator: string;
    priority: string;
    resolution: number;
    report: boolean;
    interference?: string;
    subcategoryItfc?: string;
    informer: string;
    offices: string[];
    associate: boolean;
}