export interface InterferenceCategory {
	id: number;
	description: string;
}

export interface InterferenceSubCategory {
	id: number;
	description: string;
	categoryId: number;
}


/**
 * interference y risk factor tienen las mismas características. 
 * Debería ser una misma interfaz con un nombre mas genérico.
 */