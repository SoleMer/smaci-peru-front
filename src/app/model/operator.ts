export interface Operator {
    id: number;
    name: string;
    lastname: string;
    typeDocument: string; 
    numberDocument: string; 
    email: string;
}