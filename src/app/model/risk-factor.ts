export interface RiskFactorCategory {
	id: number;
	description: string;
}

export interface RiskFactorSubCategory {
	id: number;
	description: string;
	categoryId: number;
}
